# SpdXtra Demo/Tutorial #

This repository serves to both explain and demonstrate the features of [SpdXtra](http://github.com/yevster/spdxtra). All demo code is provided under the [MIT License](https://bitbucket.org/yevster/spdxtraxample/src/HEAD/LICENSE.txt?at=master).

### Browse the example(s) ###
[Describing a package with a static dependency](https://bitbucket.org/yevster/spdxtraxample/src/HEAD/src/java/com/yevster/example/PackageDependencyOnly.java?at=master&fileviewer=file-view-default). [(Result)](https://bitbucket.org/snippets/yevster/5Kzyx)

[Describing a package with file contents](https://bitbucket.org/yevster/spdxtraxample/src/HEAD/src/java/com/yevster/example/FilesInPackage.java?at=master&fileviewer=file-view-default). [(Result)](https://bitbucket.org/snippets/yevster/aKbaE)

### Building the Project ###
Clone the repo and run `./gradlew assemble` to build.