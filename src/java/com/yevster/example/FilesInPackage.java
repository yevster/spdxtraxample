package com.yevster.example;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.junit.Test;

import com.yevster.spdxtra.LicenseList;
import com.yevster.spdxtra.NoneNoAssertionOrValue;
import com.yevster.spdxtra.Read;
import com.yevster.spdxtra.Write;
import com.yevster.spdxtra.Write.ModelUpdate;
import com.yevster.spdxtra.model.Checksum;
import com.yevster.spdxtra.model.Creator;
import com.yevster.spdxtra.model.FileType;
import com.yevster.spdxtra.model.write.License;

public class FilesInPackage {
	public static final String BASE_URL = "http://nullpointerfactory.wordpress.com/exampledocument";

	@Test
	public void createPkgWithFile() {
		/*
		 * For demonstration purposes only. For larger or longer-lasting
		 * projects,, use a hard disk-based dataset:
		 * TDBFactory.createDataset(...)
		 */
		Dataset dataset = DatasetFactory.createTxnMem();

		/**
		 * Create the document and package. See PackageDependencyOnly for
		 * explanations of package attributes.
		 */
		final String baseUrl = "http://nullpointerfactory.wordpress.com/exampledocument";
		final String documentSpdxId = "SPDXRef-MyAwesomeness";
		final String packageSpdxId = "SPDXRef-Package";

		Write.applyUpdatesInOneTransaction(dataset,
				Write.New.document(baseUrl, documentSpdxId, "My Awesome SPDX Document of awesomeness",
						Creator.person("Pat the Packager", Optional.of("pat@packagepackagers.pkg")),
						Creator.tool("SpdXtra Demo")),
				Write.Document.addDescribedPackage(baseUrl, documentSpdxId, packageSpdxId,
						"My Awesome Software Package"),
				Write.Package.declaredLicense(baseUrl + "#" + packageSpdxId,
						LicenseList.INSTANCE.getListedLicenseById("Apache-2.0").get()),
				Write.Package.concludedLicense(baseUrl + "#" + packageSpdxId, License.NOASSERTION),
				Write.Package.copyrightText(baseUrl + "#" + packageSpdxId,
						NoneNoAssertionOrValue.of("Copyright (c) 2016 Package Packagers, Inc.")));

		// Generate updates to define a single file inside the package.
		// Important: when files are defined for a package, the filesAnalyzed
		// attribute on that package must
		// be set to true or omitted. So we will omit it here:

		// Well done. Now, let's build up the updates for the file.
		final String fileSpdxId = "SPDXRef-MyFile";
		// As with packages, files have SPDX IDs, in the form SPDXRef-*. And as
		// with packages, the URI for the file is the document base URL + '#' +
		// the file SPDX ID.

		List<ModelUpdate> fileUpdates = new LinkedList<>();

		// Create the file inside the package:
		fileUpdates.add(Write.Package.addFile(baseUrl, packageSpdxId, fileSpdxId, "./myFile.txt"));
		// Note above that the file name began with ".". This is the standard
		// for all SPDX files - the file name is actually a path relative to the
		// top project directory, so it starts with "./".

		// This is obviously a test file, so let's set its file type.
		fileUpdates.add(Write.File.fileTypes(baseUrl + "#" + fileSpdxId, FileType.TEXT));

		// Every file must have at least a SHA1 checksum. Select other
		// checksums, such as MD5 can also be added.
		String sha1digest = "43a97316c6941c2f9de24185e4ad2b44f16a56ae";
		String md5digest = "69253fc5ee40567855acb6edbda10e68";
		fileUpdates.add(Write.File.checksums(baseUrl + "#" + fileSpdxId, sha1digest, Checksum.md5(md5digest)));

		// Every file has its own concluded license. In our case, give the file
		// the same concluded license as our project.
		fileUpdates.add(Write.File.concludedLicense(baseUrl + "#" + fileSpdxId,

				LicenseList.INSTANCE.getListedLicenseById("Apache-2.0").get()));
		// If the file actually contains a license text, whether the license is
		// the one declared for the file or not, that license is specified in
		// this dandy attribute:
		fileUpdates.add(Write.File.addLicenseInfoInFile(baseUrl + "#" + fileSpdxId,
				LicenseList.INSTANCE.getListedLicenseById("Apache-2.0").get()));

		// Sometimes a file contains the texts of multiple licenses (like the
		// iTunes License Agreement). In that scenario, you'd have to use the
		// addLicenseInfoInFile method more than once.

		// If the license for the file is different than the license for the
		// project, you generally want to explain why in the "comments on
		// license" field:
		fileUpdates.add(Write.File.licenseComments(baseUrl + "#" + fileSpdxId,
				"Don't really need a comment here, because this file has the same license as "
						+ "its containing package. But this is a demo, ain't it?"));

		// Copyright text is mandatory, as with packages. If there is no
		// copyright information in the file, we can use the NONE value.
		// Since we don't know the copyright information in the file because we
		// didn't look inside (pretend we didn't assert earlier that the file
		// contains the Apache 2.0 license), we'll use the NOASSERTION value.
		fileUpdates.add(Write.File.copyrightText(baseUrl + "#" + fileSpdxId, NoneNoAssertionOrValue.NO_ASSERTION));

		// Let's credit the person and/or organization that contributed this
		// file.
		fileUpdates.add(Write.File.contributors(baseUrl + "#" + fileSpdxId, "Foosey McFoozlesticks, esq.",
				"Foosey Boosey, Inc. Ltd."));

		// We have now fully described the properties of our new file.
		// But, we're still not done! To determine whether the package contents
		// on disk match the contents documented in the SPDX document, an SPDX
		// package with file contains a "verification code", derived from the
		// SHA1 checksums of the files. SpdXtra can compute this code for us, we
		// just need to tell it that we're done adding files to this package.
		fileUpdates.add(Write.Package.finalize(baseUrl + "#" + packageSpdxId));

		// That's it, let's make all this happen!
		Write.applyUpdatesInOneTransaction(dataset, fileUpdates);

		try {
			Path outputPath = Paths.get(System.getProperty("java.io.tmpdir"), "pkgWithFile.rdf");
			System.out.println("Enjoy your SPDX file at " + outputPath.toAbsolutePath().toString());
			Read.outputRdfXml(dataset, outputPath);
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

}
