package com.yevster.example;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.junit.Test;

import com.yevster.spdxtra.LicenseList;
import com.yevster.spdxtra.NoneNoAssertionOrValue;
import com.yevster.spdxtra.Read;
import com.yevster.spdxtra.Write;
import com.yevster.spdxtra.Write.ModelUpdate;
import com.yevster.spdxtra.model.Creator;
import com.yevster.spdxtra.model.Relationship;
import com.yevster.spdxtra.model.write.License;

/**
 * @author yevster SPDX-License-Identifier: MIT
 *         <p>
 *         This file demonstrates how to build an Spdx Document with SpdXtra,
 *         providing an overview of both the SpdXtra API and the structure and
 *         requirements of the SPDX document format.
 */
public class PackageDependencyOnly {

	@Test
	public void createSampleDocument() {
		/*
		 * For demonstration purposes only. For larger or longer-lasting
		 * projects,, use a hard disk-based dataset:
		 * TDBFactory.createDataset(...)
		 */
		Dataset dataset = DatasetFactory.createTxnMem();

		// To create a document, we're going to build up a list of operations
		// that will populate this dataset.
		List<ModelUpdate> documentCreation = new LinkedList<>();
		/*
		 * First we create a new document. Every SPDX document needs a base URL
		 * that can be used to uniquely identify parts of this document when
		 * referenced from other SPDX documents (it can happen!). We'll use
		 * http://nullpointerfactory.wordpress.com/exampledocument. The document
		 * also needs an SPDX ID that MUST (per the SPDX spec) start with
		 * "SPDXRef-". The name is just a human-friendly name.
		 */
		final String baseUrl = "http://nullpointerfactory.wordpress.com/exampledocument";
		final String documentSpdxId = "SPDXRef-MyAwesomeness";

		documentCreation.add(Write.New.document(baseUrl, documentSpdxId, "My Awesome SPDX Document of awesomeness",
				Creator.person("Borat Sagdiev", Optional.of("borat@moviefilmstudio.kz")),
				Creator.tool("SpdXtra Demo")));
		/*
		 * Now, we'll create a package to represent our project. The document
		 * above will have a DESCRIBES relationship to that package. Just like
		 * documents, packages have their own SPDX identifiers that can be
		 * referenced from inside or outside the document.
		 */
		final String packageSpdxid = "SPDXRef-BundleOfJoy1.0";

		documentCreation
				.add(Write.Document.addDescribedPackage(baseUrl, documentSpdxId, packageSpdxid, "Bundle of Joy 1.0"));
		/*
		 * We'll need to set some properties for our package. But we haven't
		 * even applied the update for creating the package. So we'll have to
		 * refer to the future package by its URI. That's simple enough: the URI
		 * of any SPDX element is the concatenation of the base document URL,
		 * the character '#' and the element's SPDX identifier:
		 */
		final String myPackageUri = baseUrl + '#' + packageSpdxid;

		/*
		 * SPDX packages aren't required to have versions, but it's a good
		 * practice to set one.
		 */
		documentCreation.add(Write.Package.version(myPackageUri, "0.0.1-pre-alpha-do-not-use-worse-than-Vista"));

		/*
		 * Let's set the copyright text for our package. It is a required field,
		 * but SpdXtra populates it with the NOASSERTION value by default.
		 */
		documentCreation.add(Write.Package.copyrightText(myPackageUri,
				NoneNoAssertionOrValue.of("Copyright (c) 2016 Magical Forest Creatures, LLC. Inc. BS.")));

		/*
		 * There's one last field required by the SPDX spec for a package: the
		 * download location. This can refer to a location on the web or in a
		 * version control system. SpdXtra sets this value to "NOASSERTION" by
		 * default, but we can provide something far more useful.
		 */
		documentCreation.add(
				Write.Package.packageDownloadLocation(myPackageUri, NoneNoAssertionOrValue.of("http://bit.ly/IqT6zt")));

		// Communicating license information is the bread and butter of SPDX.
		// Let's define our license.
		String customLicenseText = "In exchange for using this software, you agree to give its author all your worldly posessions.\n"
				+ "You will not hold the author liable for all the damage this software will inevitably cause\n"
				+ "not only to your person and property, but to the entire fabric of the cosmos.\n";
		// An "Extracted License" is a license whose text is contained in a
		// file, rather than in the standard license list. It's basically an
		// SPDX term meaning a custom license. A custom license has an
		// SPDX ID and should share the base URL of the document that
		// contains it. SPDX IDs for licenses must start with "LicenseRef-".
		License extractedLicense = License.extracted(customLicenseText, "Eternal Surrender License", baseUrl, "LicenseRef-EternalSurrender");

		// We can set the license above as the license for our package.
		// But in an unexpected display of kindness, we'll give our user a
		// choice between the license above and GPL 2.0. To do that, we'll
		// create a "Conjunctive License" - this is a license that lets the
		// consumer choose between two or more licenses.
		License conjunctiveLicense = License.or(extractedLicense,
				LicenseList.INSTANCE.getListedLicenseById("GPL-2.0").get());

		documentCreation.add(Write.Package.declaredLicense(myPackageUri, conjunctiveLicense));
		/*
		 * A declared license, as added above, is the license that the author of
		 * the package "declares" to be binding for that package. But someone
		 * who has not authored the package could scan it with a scanning tool
		 * and conclude that a different license or combination of licenses
		 * should apply. That's what the "Concluded License" field is for. We
		 * haven't scanned our package, so we'll use the "NOASSERTION" value.
		 */
		documentCreation.add(Write.Package.concludedLicense(myPackageUri, License.NOASSERTION));

		/*
		 * In this example, we're going to describe the package without listing
		 * its actual file contents. To do that, we need to set the
		 * filesAnalyzed on that package to false.
		 */
		documentCreation.add(Write.Package.filesAnalyzed(myPackageUri, false));
		/*
		 * At last, let's apply all these updates we've piled up and actually
		 * generate our SPDX model
		 */
		Write.applyUpdatesInOneTransaction(dataset, documentCreation);

		/*
		 * Let's pretend our package has a static (compile-time) dependency on
		 * another package, let's say Apache Commons Lang. To describe that
		 * dependency, we add another package to our document describing Apache
		 * Commons Lang.
		 */
		List<ModelUpdate> commonsLangPackageOperations = new LinkedList<>();
		String commonsSpdxId = "SPDXRef-ApacheCommonsLang-3.4";
		commonsLangPackageOperations
				.add(Write.Document.addPackage(baseUrl, documentSpdxId, commonsSpdxId, "Apache Commons Lang"));
		String commonsLangPackageUri = baseUrl + "#" + commonsSpdxId;
		commonsLangPackageOperations.add(Write.Package.version(commonsLangPackageUri, "3.4"));
		commonsLangPackageOperations.add(Write.Package.packageDownloadLocation(commonsLangPackageUri,
				NoneNoAssertionOrValue.of("http://commons.apache.org/lang/download_lang.cgi")));
		commonsLangPackageOperations.add(Write.Package.homepage(commonsLangPackageUri,
				NoneNoAssertionOrValue.of("http://commons.apache.org/lang/")));

		/*
		 * The License.txt file inside Apache Commons Lang indicates it's
		 * licensed under the Apache 2.0 License. This is the declared license.
		 * Since we have not analyzed the full contents of the Commons Lang
		 * package to come to our own conclusion whether this or any other
		 * license applies, we'll specify NOASSERTION for the concluded license.
		 */
		commonsLangPackageOperations.add(Write.Package.declaredLicense(commonsLangPackageUri,
				LicenseList.INSTANCE.getListedLicenseById("Apache-2.0").get()));
		commonsLangPackageOperations.add(Write.Package.concludedLicense(commonsLangPackageUri, License.NOASSERTION));

		/*
		 * As before, we have to explicitly state that we have not analyzed the
		 * contents of this package. Otherwise, consumers of our document may
		 * find the document fails to validate without additional information
		 * that we're not providing here.
		 */
		commonsLangPackageOperations.add(Write.Package.filesAnalyzed(commonsLangPackageUri, false));

		/*
		 * Now that we have a definition for the Apache Commons Lang package,
		 * let's define the relationship it has to our main package. Because
		 * it's a static dependency, we'll use a static link relationship.
		 */
		commonsLangPackageOperations.add(Write.addRelationship(myPackageUri, commonsLangPackageUri,
				Optional.of("Apache Commons Lang is the best library in the world. Why would you NOT use it?"),
				Relationship.Type.STATIC_LINK));

		/*
		 * Now that we have everything we need to add our new package and relate
		 * it to our main package, let's write all this into our dataset in one
		 * fell swoop (i.e. transaction)
		 */
		Write.applyUpdatesInOneTransaction(dataset, commonsLangPackageOperations);

		/*
		 * Our SPDX is now in our dataset.
		 */

		try {
			Path outputPath = Paths.get(System.getProperty("java.io.tmpdir"), "pkgDepsOnly.rdf");
			System.out.println("Enjoy your SPDX file at " + outputPath.toAbsolutePath().toString());
			Read.outputRdfXml(dataset, outputPath);
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

}
